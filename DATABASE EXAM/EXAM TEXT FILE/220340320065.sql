
/*Q1. Create table DEPT with the following structure:-
DEPTNO int(2)
DNAME varchar(15)
LOC varchar(10)

Insert the following rows into the DEPT table:-
10 ACCOUNTING NEW YORK
20 RESEARCH DALLAS
30 SALES CHICAGO
40 OPERATIONS BOSTON

*/

CREATE TABLE DEPTT(DEPTNO int(2), DNAME varchar(15), Loc varchar(10));
select * from DEPTT;
INSERT INTO DEPTT VALUES 
( 10, 'ACCOUNTING' , 'NEW YORK'),
( 20, 'RESEARCH' , 'DALLAS'),
( 30, 'SALES' , 'CHICAGO'),
( 40, 'OPERATIONS' , 'BOSTON');





/*Q2. Create table EMP with the following structure:-
EMPNO int(4)
ENAME varchar(10)
JOB varchar(9)
HIREDATE date
SAL float(7,2)
COMM float(7,2)
DEPTNO int(2)
Insert the following rows into the EMP table:-
7839 KING MANAGER 1991-11-17 5000 NULL 10
7698 BLAKE CLERK 1981-05-01 2850 NULL 30
7782 CLARK MANAGER 1981-06-09 2450 NULL 10
7566 JONES CLERK 1981-04-02 2975 NULL 20
7654 MARTIN SALESMAN 1981-09-28 1250 1400 30
7499 ALLEN SALESMAN 1981-02-20 1600 300 30*/

CREATE TABLE EMPP( EMPNO int(4),ENAME varchar(10),JOB varchar(9),HIREDATE date,SAL float(7,2),COMM float(7,2),DEPTNO int(2));
select * from EMPP;

INSERT INTO EMPP VALUES
(7839 , 'KING' , 'MANAGER' , '1991-11-17' , 5000, NULL ,10),
(7698 , 'BLAKE' , 'CLERK' , '1981-05-01 ' , 2850, NULL ,30),
(7782 , 'CLARK' , 'MANAGER' , '1981-06-09' , 2450, NULL ,10),
(7566 , 'JONES' , 'CLERK' , '1981-04-02' , 2975, NULL ,20),
(7654 , 'MARTIN' , 'SALESMAN' , '1981-09-28' , 1250, 1400 ,30),
(7499 , 'ALLEN' , 'SALESMAN' , '1981-02-20' , 1600, 300 ,30);

DELETE FROM empp;

/*3. Display all the employees where SAL between 2500 and 5000 (inclusive of both).
4. Display all the ENAMEs in descending order of ENAME.
5. Display all the JOBs in lowercase.
6. Display the ENAMEs and the lengths of the ENAMEs
7. Display the DEPTNO and the count of employees who belong to that DEPTNO .
8. Display the DNAMEs and the ENAMEs who belong to that DNAME.
9. Display the position at which the string ‘AR’ occurs in the ename.
10. Display the HRA for each employee given that HRA is 20% of SAL*/

SELECT * FROM EMPP WHERE Sal BETWEEN 2500 AND 5000;
SELECT ENAME FROM EMPP ORDER BY ENAME DESC;
SELECT LOWER(JOB) FROM EMPP;
SELECT ENAME , LENGTH(ENAME) AS LENGTH FROM EMPP;
SELECT DEPTNO, COUNT(*)  AS "EMPLOYEE COUNT" FROM EMPP GROUP BY DEPTNO;
SELECT DEPTNO , DNAME , ENAME FROM  DEPTT , EMPP WHERE DEPTT.DEPTNO = EMPP.DEPTNO ;
SELECT INSTR( ENAME, 'AR') AS P FROM EMPP;
SELECT ENAME , SAL*0.2 AS HRA FROM EMPP;





/*2. Create a stored function by the name of FUNC1 to take three parameters, the 
sides of a triangle. The function should return a Boolean value:- TRUE if the 
triangle is valid, FALSE otherwise. A triangle is valid if the length of each side is 
less than the sum of the lengths of the other two sides. Check if the dimensions 
entered can form a valid triangle. Calling program for the stored function need not 
be written*/

 delimiter //
 create function FUNC1 ( X INT , Y INT , Z INT)
 returns boolean 
 deterministic
 begin
        if (X+Y>Z) and (Y+Z>X) and (Z+X>Y) then
           return true;
	    else 
           return false;
		end if;
 end //
 delimiter ;

select FUNC1(1,2,3);
select FUNC1(5,5,5);



/*1. Write a stored procedure by the name of PROC1 that accepts two varchar strings
as parameters. Your procedure should then determine if the first varchar string 
exists inside the varchar string. For example, if string1 = ‘DAC’ and string2 = 
‘CDAC, then string1 exists inside string2. The stored procedure should insert the 
appropriate message into a suitable TEMPP output table. Calling program for the 
stored procedure need not be written.*/

CREATE TABLE TEMPP(S1 VARCHAR(15) , S2 VARCHAR(25), RESULT VARCHAR(40));
DROP TABLE TEMPP;
delimiter //
create procedure PROC1(string1 varchar(15) , string2 varchar(15))
begin 
   if ( instr(string1 , string2)>0 or instr(string2, string1)>0) then
       insert into TEMPP values(string1 , string2 , "string1 exists inside string2");
   else
	   insert into TEMPP values(string1 , string2 , "string1  not exists inside string2");
       
   end if;
end; //
delimiter ;

call PROC1('DAC','CDAC');   
SELECT * FROM TEMPP;















